import numpy as np

from ReplicatorII import Replicator, PetriDish

class ReplicatorTester(PetriDish):
	def __init__(self):
		self.verbose = True
		self.getpars()

	def getpars(self):
		if not hasattr(self, 'defaultPars'):
			r = Replicator('local')
			self.defaultPars = r.pars

		self.pars = {}
		for p in self.defaultPars:
			self.pars[p] = self.defaultPars[p]

	def getsimplecells(self):
		cells = [
			[1,1,0,0],
			[2,1,0,0],
			[1,0,0,0],
		]

		self.cells = np.array(cells)
		self.n = 3
		self.zeros = 0.5

		# Sample relies on initial-distribution-n and on self.n_signals_per_plasmid
		self.pars['initial-distribution-n'] = 3
		self.n_signals_per_cell = np.array([2,2,1])
		self.n_plasmids_per_cell = np.array([2,3,1])

	# Functions to be tested

	# ---------------------------------------------------------------------------------- #
	# MAIN SIM FUNCTIONS

	# simulate(self)
	# s_m_phase(self)
	# sphase(self, i)
	# mphase(self, i, clusters)
	def test_mphase(self):


	# g1phase(self)
	def test_g1phase(self):
		self.getsimplecells()
		self.pars['cluster-jostling'] = 1.0
		self.pars['cluster-crp-alpha'] = 100

		expected = np.array([
			[1,1,0,0],
			[1,1,1,0],
			[1,0,0,0],
		])

		self.g1phase()
		print self.cells
		if not(np.array_equal(self.cells, expected)): return False

		self.pars['cluster-jostling'] = 0.4
		val = 0
		for i in range(1000):
			self.getsimplecells()
			self.g1phase()
			bins = self._current_signals_per_cell()
			if np.array_equal(bins, [0.5, 1.0/3, 1.0/3, 1.0/3]): val += 2.0/1000
			elif np.array_equal(bins, [0.5, 1.0/3, 2.0/3]): val += 1.0/1000
			else: val += 10000

		if val < 1.35 or val > 1.45: return False
		return True

	# ---------------------------------------------------------------------------------- #
	# BIOLOGICAL CLUSTERING FUNCTIONS

	# def _pad(self, a, b)
	def test__pad(self):
		# Guaranteed to be correct if s_phase_cluster_breakup works. Also, tested recently
		return True

	# s_phase_cluster_breakup(self, clusters)
	def test_s_phase_cluster_breakup(self):
		clusters = np.array([
			[10, 0, 1, 0],
			[0, 11, 1, 0],
		])

		self.pars['cluster-jostling'] = 1.0
		self.pars['cluster-jostling-s-vs-g1'] = 0.0
		self.pars['cluster-crp-alpha'] = 1.0

		expected = np.array([
			[10, 0, 1],
			[0, 11, 1],
		])
		if not(np.array_equal(expected, self.s_phase_cluster_breakup(clusters))): return False

		self.pars['cluster-jostling-s-vs-g1'] = 1.0
		self.pars['cluster-crp-alpha'] = 100


		clusters = np.array([
			[2, 0, 1, 0],
			[0, 3, 1, 0],
		])
		expected = np.array([
			[1, 1, 0, 0, 0, 1],
			[0, 0, 1, 1, 1, 1],
		])
		if not(np.array_equal(expected, self.s_phase_cluster_breakup(clusters))): return False
		return True

	# dissociate_cluster_and_jostle(self, n, sphase=True)
	def test_dissociate_cluster_and_jostle(self):
		if self.dissociate_cluster_and_jostle(0, True) != []: return False
		if self.dissociate_cluster_and_jostle(0, False) != []: return False
		if self.dissociate_cluster_and_jostle(1, True) != [1]: return False
		if self.dissociate_cluster_and_jostle(1, False) != [1]: return False

		self.pars['cluster-jostling'] = 1.0
		self.pars['cluster-jostling-s-vs-g1'] = 0.0
		self.pars['cluster-crp-alpha'] = 100

		if self.dissociate_cluster_and_jostle(10, True) != [10]: return False
		if self.dissociate_cluster_and_jostle(10, False) != [1]*10: return False

		return True

	# crp(self, n, alpha)
	def test_crp(self):
		for i in range(1, 500):
			bins = self.crp(i, 100)
			if np.max(bins) > 1 or len(bins) != i: return False

		print 'Have checked CRP for consistency, but not against expected value. This should not matter much for the simulation.'

		return True

	# _crp_occupied(self, t, n, alpha)
	def test__crp_occupied(self):
		# Only crp calls _crp_occupied. Therefore, if _crp passes, so does this
		return True

	# ---------------------------------------------------------------------------------- #
 	# SET UP FUNCTIONS

	# population(self)
	def test_population(self):
		self.population()
		spc = self._current_signals_per_cell()
		pps = self.get_plasmids_per_signal()
		if len(pps) != 1: return False
		if np.argmax(spc) > self.pars['initial-distribution-x'] + 1 or np.argmax(spc) < self.pars['initial-distribution-x'] - 1: return False
		return True

	# sample(self)
	def test_sample(self):
		# Test for correct shuffling
		mn = 0
		for i in range(1000):
			self.getsimplecells()
			self.pars['initial-distribution-n'] = 1
			self.sample()
			bins = self._current_signals_per_cell()
			if len(bins) == 3: mn += 2/1000.0
			elif len(bins) == 2: mn += 1/1000.0
			else: mn += 10000
		if mn < 1.6 or mn > 1.73: return False

		# Test for correct addition of zeros
		self.getsimplecells()
		self.pars['initial-distribution-n'] = 1
		self.cells[1,:] = 0
		self.n = 2
		self._reset_counts_per_cell()
		self.sample()
		if self.zeros != 0.75: return False

		return True

	#_reset_counts_per_cell is tested within sample. I split it out during the creation
	# of this testing suite.

	# _burnin(self)
	def test__burnin(self):
		# Checked by eye. Works if sample works. Resets zeros
		return True

	# _set_selection_for_live_imaging_comparison(self)
	def test__set_selection_for_live_imaging_comparison(self):
		print 'Difficult and unnecessary to test. Will not publish results.'
		return False

	# _get_selected_daughters(self)
	def test__get_selected_daughters(self):
		print 'Difficult and unnecessary to test. Will not publish results.'
		return False

	# ---------------------------------------------------------------------------------- #
 	# OUTPUT FUNCTIONS

	# _current_signals_per_cell(self)
	def test__current_signals_per_cell(self):
		# Depends on self.sample and the correct setting of self.n_signals_per_cell
		# Sample sets both
		self.getsimplecells()
		bins = self._current_signals_per_cell()
		if np.array_equal(bins, [0.5, 1.0/3.0, 2.0/3.0]): return True
		else: return False


	# get_signals_per_cell(self, init_or_final='final')
	def test_get_signals_per_cell(self):
		# Checked by eye. Just multiplies a copy of initpop by 100 or of the current pop.
		# Init pop is copied to make sure that we can retrieve it again if necessary
		return True

	# get_plasmids_per_signal(self)
	def test_get_plasmids_per_signal(self):
		self.getsimplecells()
		bins = self.get_plasmids_per_signal()
		if bins[0] == 80.0 and bins[1] == 20.0 and len(bins) == 2: return True
		else:
			raise Exception('Bins are at %f, %f' % (bins[0], bins[1]))
			return False

	# get_daughter_to_parent_signal_ratio(self)
	def test_get_daughter_to_parent_signal_ratio(self):
		# Checked by eye - simple function
		# Says "signal", not "plasmid"
		return True

	# get_daughter_to_parent_plasmid_ratio(self)
	def test_get_daughter_to_parent_plasmid_ratio(self):
		# Checked by eye - simple function
		# Says "plasmid", not "signal"
		return True

	def test(self):
		print ''
		print '='*60
		testFunctions = [
			'simulate',
			's_m_phase',
			'sphase',
			'mphase',
			'g1phase',
			'_pad',
			's_phase_cluster_breakup',
			'dissociate_cluster_and_jostle',
			'crp',
			'_crp_occupied',
			'population',
			'sample',
			'_burnin',
			'_set_selection_for_live_imaging_comparison',
			'_get_selected_daughters',
			'_current_signals_per_cell',
			'get_signals_per_cell',
			'get_plasmids_per_signal',
			'get_daughter_to_parent_signal_ratio',
			'get_daughter_to_parent_plasmid_ratio',
		]

		for fun in testFunctions:
			if hasattr(self, 'test_' + fun):
				self.getpars()
				print 'Testing function %s...' % (fun)
				tf = getattr(self, 'test_' + fun)()
				if tf: print '\t passed.'
				else: print '\t failed.'

		print ''

if __name__ == "__main__":
	rt = ReplicatorTester()
	rt.test()
