from parse import Parser
from math import sqrt, ceil, floor
from sys import argv
from re import match as rematch
import numpy as np
import os.path as opath
from os import listdir, mkdir
import matplotlib as mpl
mpl.use('Agg', warn=False)
import matplotlib.pyplot as plt
#from matplotlib.path import Path
#import matplotlib.gridspec as gridspec

class Grapher():
	def __init__(self, dir, scale_nonzeros=True, plot_inits=True):
		self.path = dir if dir[-1] == '/' else dir + '/'
		self.scalenzs = scale_nonzeros
		
		self.parse = Parser()
		self.keys()
		print self.varying
		
	# KEYS
	# Read the varying keys
	def keys(self):
		fp = open(self.path + 'keys.txt')
		ks = fp.read().split('\n')
		fp.close()
		
		self.varying = []
		found = False
		for line in ks:
			if not found and len(line) > 13 and line[:13] == '# --- Varying': found = True
			elif found:
				if len(line) > 1 and line.find(':') > -1:
					key, val = line.split(':')
					self.varying.append((key, self.parse.tuple(val)))
				else: found = False
		
	# READ
	# Read in a single simulation
	def read(self, searchtext='final'):
		title = []
		for i in range(len(self.varying)): title.append('%s-%g' % ('abcdefghijklmnopqrstuvwxyz'[i], self.pars[self.varying[i][0]]))
		if not opath.exists(self.path + ', '.join(title) + '.txt'): return []
		fp = open(self.path + ', '.join(title) + '.txt')
		lines = fp.read().split('\n')
		fp.close()
		
		found = False
		path = []
		for line in lines:
			if found and rematch('^[\d]+\t[\d]+(\.[\d]+)?$', line): path.append(float(line.split('\t')[1]))
			elif not found and line.find('#') > -1 and line.lower().find(searchtext) > -1: found = True
			elif found and line.find('#') > -1 and line.find('# ---') == -1: found = True
			else: found = False
		
		return np.array(path)
		
	# VARY PARAMETERS
	# Vary the varying parameters with a recursive function
	def vary(self, n=0, skip=-1):
		if n == skip: self.vary(n + 1, skip)
		
		elif n < len(self.varying):
			x = self.varying[n][1][0]
			y = self.varying[n][1][1]
			s = self.varying[n][1][2]
			
			while x <= y:
				self.pars[self.varying[n][0]] = x
				self.vary(n + 1, skip)
				x += s					
			
		elif skip > -1:
			paths = []
			rpath = 0
			x = self.varying[skip][1][0]
			y = self.varying[skip][1][1]
			s = self.varying[skip][1][2]
			
			while x <= y:
				self.pars[self.varying[skip][0]] = x
				paths.append(self.read())
				if len(paths[-1]) > 0: rpath += 1
				x += s
				
			if rpath > 1:
				condition = []
				for i in range(len(self.varying)):
					if i != skip: condition.append(self.pars[self.varying[i][0]])
				self.ys.append((condition, paths))
				
		else:
			condition = [self.pars[v[0]] for v in self.varying]
			fpop = self.read('final')
			if len(fpop) > 0:
				ipop = self.read('initial')
				self.ys.append((condition, fpop, ipop))
			
	# GRAPH PARAMETERS
	# Make the set of graphs
	def graph_parameters(self):
		self.font()
		for m in range(len(self.varying)):
			self.ys = []
			self.pars = {}
			self.vary(skip = m)
			
			if len(self.ys) > 0:
				ii = int(ceil(sqrt(len(self.ys))))
				jj = int(ceil(len(self.ys)/ii))
			
				print '\t' + self.varying[m][0].replace('-', ' ')
				size = (13, float(jj)/ii*12. + 1)
				fig, axes = plt.subplots(figsize=size, ncols=ii, nrows=jj)
				fig.suptitle('VARYING %s' % (self.varying[m][0].upper().replace('-', ' ')))
				
				for i in range(ii):
					for j in range(jj):
						p = i + (j*ii)
						if p < len(self.ys):
							if jj > 1: self.graph_parameter(axes[j][i], p)
							elif ii > 1: self.graph_parameter(axes[i], p)
							else: self.graph_parameter(axes, p)
						
				plt.savefig(self.path + self.varying[m][0] + '.pdf')
				plt.close(fig)

	# GRAPH PARAMETER
	# Draw a single graph
	def graph_parameter(self, ax, p):
		ax.set_ylim(0, 100)
		ax.set_frame_on(False)
		ax.get_xaxis().set_visible(False)
		ax.axes.get_yaxis().set_ticks([])
		ax.hold(True)
		
		tmx = -1
		if self.scalenzs:
			for i in range(len(self.ys[p][1])):
				if len(self.ys[p][1][i]) > 1:
					mx = np.max(self.ys[p][1][i][1:])
					if mx > tmx: tmx = mx
		
		for i in range(len(self.ys[p][1])):
			if len(self.ys[p][1][i]) > 1:
				if self.scalenzs: self.ys[p][1][i][1:] = 100*(self.ys[p][1][i][1:]/tmx)
				ax.plot(self.ys[p][1][i], c=self.color(i, len(self.ys[p][1])), lw=0.5)

	# GRAPH BEFORE AND AFTER
	# Graph the starting distribution and the final distribution
	def graph_before_after(self):
		self.font()
		self.ys = []
		self.pars = {}
		self.vary()
		print self.varying, self.ys
		
		if len(self.ys) > 0:
			if not opath.exists(self.path + 'graphs'): mkdir(self.path + 'graphs')
			
			for g in self.ys:
				fig = plt.figure(figsize=(12.25, 6))
				ax = plt.subplot(111)
				self.simpleaxis(ax)
				
				ax.plot(g[2], c='#666666', lw=1)
				ax.plot(g[1], c=self.color(5), lw=1)
				ax.set_xlim([0, 60])
				
				title = []
				for i in range(len(g[0])): title.append('%s-%g' % ('abcdefghijklmnopqrstuvwxyz'[i], g[0][i]))
				plt.savefig(self.path + 'graphs/' + ', '.join(title) + '.pdf')
				plt.close(fig)
		
	# COLOR
	# Return color based on number
	def color(self, i, mx=-1):
		cs = [
			'D8F462',
			'5DD630',
			'23E8D0',
			'3DD9EA',
			'28AFF9',
			'1F7BE8',
			'2951C1',
			'8E33DD',
			'C38DFC',
			'C46ED3',
		]
		if mx > -1 and mx < 5: return '#' + cs[(2*i + 1)%len(cs)]
		else: return '#' + cs[i%len(cs)]
		
	# FONT
	# Sets font to Gotham and linewidths to narrow
	def font(self, title=False):
		fnt = {'family':'Gotham', 'weight':'light', 'size':16}
		mpl.rc('font', **fnt)
		mpl.rcParams['lines.linewidth'] = 0.75
		return self
		
	# INTERNAL METHOD: SIMPLE AXIS
	# Set axis to look like my axes from Keynote into Illustrator
	def simpleaxis(self, ax):
		ax.yaxis.grid(True, linestyle='solid', linewidth=0.75, color='#AAAAAA')
		ax.tick_params(axis='x', pad = 15)
		ax.tick_params(axis='y', pad = 15)
		ax.spines['top'].set_visible(False)
		ax.spines['right'].set_visible(False)
		ax.spines['left'].set_visible(False)
		ax.get_xaxis().tick_bottom()
		ax.get_xaxis().set_tick_params(direction='out')
		ax.get_yaxis().set_ticks_position('none')
		ax.set_axisbelow(True)
		ax.xaxis.labelpad = 15
		ax.yaxis.labelpad = 15
		return ax

			
if __name__ == "__main__":
	if len(argv) > 1 and opath.exists(argv[-1]): g = Grapher(argv[-1])
	else:
		print '\tCall with python grapher.py [option] [path-to-directory]\n\tOptions are parameters or before-after'
		exit(0)
	
	if len(argv) > 2 and argv[1].lower()[:3] == 'par': g.graph_parameters()
	elif len(argv) > 2 and argv[1].lower()[:3] == 'bef': g.graph_before_after()
	else: g.graph_before_after()