# REPLICATOR II - Node-specific spartan and serial verbose codes combined
# Simulate the replication of KSHV plasmids, including clustering
# Updated: 02/19/2015 by Arthur Sugden

# Node output (out.txt):
# 1. Mean difference between initial and final populations across each number of signals
# per cell
# 2. Percentage of cells across the final population with 0 signals/plasmids
# 3. The mode of the signals/cell expressed in signals/cell (not the actual percent)
# 4. The mode of the signals/cell expressed in percentage of cells with N signals per cell
# 5. Mean number of signals/cell of the final population
# 6. Mean daughter-to-parent signal ratio across all generations after burn in (population
# should be at equilibrium after burn in)
# 7. Mean daughter-to-parent plasmid ratio across all generations after burn in

import re, time, numpy as np

#import warnings
#warnings.simplefilter("error")

# Replicator class is subclassed by the Tester suite, otherwise called from this file
class Replicator:
	def __init__(self, localornode='local'):
		# Init node or init local
		self.parallel = False if localornode.lower() == 'local' else True

		# Initialize the parameters with key:val\n pairs
		self.fpars = self.getverbosepars('replicator-crp-pars.txt')
		if 'input' in self.fpars and self.fpars['input'] == False: self.fail()

		# Copy the parameters into pars. This will be used to set the varying parameters
		self.pars = {}
		for key in self.fpars: self.pars[key] = self.fpars[key]

		# Save the best parameters
		self.bestpars = ({}, {})


	# =============================d===================================================== #
	# PARAMETERS
	# Load and check parameters

	def getverbosepars(self, path):
		"""Get parameters from input file in key:val pairs."""
		parse = Parser()
		pars = {}
		with open(path) as f:
			for line in f:
				# Skip comments and only include key, val pairs
				if len(line.strip()) > 1 and line.strip()[0] != '#' and line.find(':') > -1:
					key, val = line.split(':')
					# Add to fpars (full parameters) which will be broken down into pars later
					pars[key.strip()] = parse.unknown(val)

		return pars

	# ================================================================================== #
	# FUNCTIONS TO RUN SIMULATION
	# Load and check parameters

	def _compare_run_with_best(self, output):
		"""Compare run with other parameters that have been run. Save if best."""
		if self.bestpars[1] == {} or self._best_comparator(output) < self._best_comparator(self.bestpars[1]):
			for key in output:
				self.bestpars[1][key] = output[key]
			for key in self.pars:
				self.bestpars[0][key] = self.pars[key]

	# Return the value by which we judge if it's "best"
	def _best_comparator(self, output):
		return output['difference-from-minimal-lana1']*output['change-over-simulation']*output['difference-of-zeros-from-real-data']

	def _average_runs(self, verbose):
		"""Average simulations with the same parameters and average the results."""

		allruns = {}
		for i in range(self.pars['averaging-runs']):
			# Append the output on every run
			out = self._runonce(verbose)
			if 'failed' not in out:
				for key in out:
					if key not in allruns: allruns[key] = [out[key]]
					else: allruns[key].append(out[key])

		# Return a failure if necessary
		if allruns == {}: return {'failed': True}
		# Average each of the values before returning
		for key in allruns: allruns[key] = self._fancy_average(allruns[key])
		return allruns

	def _fancy_average(self, input):
		"""Average a list of numpy arrays or individual values."""
		if len(input) == 1: return input[0]

		# Only average if there are multiple values
		if isinstance(input[0], np.ndarray) or isinstance(input[0], list):
			# Convert and copy list of lists to list of numpy arrays
			input = [np.copy(v) for v in input]

			# Get and equalize the max length
			mx = max([len(v) for v in input])

			for i in range(len(input)): input[i].resize(mx, refcheck=False)

			# Return the mean
			return np.mean(input, axis=0)
		else: return np.mean(np.array(input))

	def _runonce(self, verbose):
		"""Run a single simulation with a single set of parameters and return the results in the correct format."""
		pd = PetriDish(self.pars, verbose)
		done = pd.simulate()

		if not done: return {'failed': True}

		# We need matching sizes to calculate the mean population change
		ipop, fpop = pd.get_signals_per_cell('initial'), pd.get_signals_per_cell('final')
		ipop.resize(max(ipop.size, fpop.size))
		fpop.resize(ipop.size)

		if len(fpop) < 2: return {'failed': True}

		signalSizes = pd.get_plasmids_per_signal()
		changes = self._distance_from_real(fpop, signalSizes, pd)
		changeOverSim = pd.pdf_distance(ipop, fpop, True)

		# Clear, explicit output
		out = {
			'initial-signals-per-cell': pd.get_signals_per_cell('initial'),
			'final-signals-per-cell': pd.get_signals_per_cell('final'),
			'mean-population-change-by-sums-depricated': np.mean(np.abs(fpop[1:] - ipop[1:])),
			'zeros': fpop[0],
			'modal-percentage-signals-per-cell': np.max(fpop[1:]),
			'mode-signals-per-cell': np.argmax(fpop[1:]) + 1,
			'mean-signals-per-cell': np.sum(fpop[1:]/100.0*[i+1 for i in range(len(fpop[1:]))]),
			'plasmids-per-signal': signalSizes,
			'daughter-parent-signals-per-cell-ratio': pd.get_daughter_to_parent_signal_ratio(),
			'daughter-parent-plasmids-per-cell-ratio': pd.get_daughter_to_parent_plasmid_ratio(),
			'change-over-simulation': changeOverSim,
			'difference-from-minimal-lana1': changes['lana1'],
			'difference-from-ebna1-lana1-fusion': changes['fusion'],
			'difference-of-zeros-from-real-data': changes['zeros'],
			'difference-signals-from-minimal-lana1': changes['signals-lana1'],
			'difference-signals-from-ebna1-lana1-fusion': changes['signals-fusion'],
			'combined-minimal-lana1-change': changes['lana1']*changes['zeros']*changeOverSim*changes['signals-lana1'],
			'combined-fusion-change': changes['fusion']*changes['zeros'],
		}

		return out

	def _distance_from_real(self, fpop, clusterSizes, pd):
		# From file updated Jan 4, 2015
		minimal_lana1 = [10.90, 10.46, 8.41, 7.23, 7.77, 6.90, 5.29, 4.10, 4.96, 3.13,
			4.53, 2.80, 3.02, 2.48, 1.62, 1.83, 1.62, 1.19, 0.97, 0.97, 0.76, 1.19, 0.65,
			0.97, 0.76, 0.32, 0.43, 0.54, 0.65, 0.22, 0.11, 0.32, 0.11, 0.32, 0.11, 0.22,
			0.43, 0.11, 0.00, 0.43, 0.11, 0.11, 0.11, 0.00, 0.11, 0.00, 0.22, 0.11, 0.11,
			0.00, 0.11, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.11, 0.11]
		ebna1_lana1_fusion = [15.02, 12.45, 10.73, 12.02, 10.30, 8.58, 6.87, 5.58, 3.43,
			2.15, 2.58, 2.15, 1.72, 1.29, 0.43, 0.86, 0.86, 1.29, 0.43, 0.00, 0.00, 0.00,
			0.43, 0.00, 0.00, 0.00, 0.00, 0.00, 0.43, 0.00, 0.00, 0.00, 0.43]
		cluster_distribution_minimal_lana1 = [34.706, 19.412, 8.824, 12.353, 5.294, 4.706,
			1.176, 2.941, 1.176, 1.176, 0.588, 0.588, 0, 0.588, 0, 0.588, 0, 0, 0.588, 0,
			0, 0.588, 0.588, 0, 0, 0, 0.588, 0, 0.588, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
			0.588, 0, 0, 0, 0, 0, 1.176, 0, 0, 0, 0, 0, 0, 0, 0.588, 0, 0, 0, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.588]
		cluster_distribution_ebna1_lana1_fusion = [100.0]
		#if pd == False: pd = PetriDish(self.pars, False)

		return {
			'lana1': pd.pdf_distance(minimal_lana1, fpop[1:], False),
			'fusion': pd.pdf_distance(ebna1_lana1_fusion, fpop[1:], False),
			'zeros': abs(0.87 - fpop[0]/100.0),
			'signals-lana1': pd.pdf_distance(cluster_distribution_minimal_lana1, clusterSizes[1:], False),
			'signals-fusion': pd.pdf_distance(cluster_distribution_ebna1_lana1_fusion, clusterSizes[1:], False),
		}



	# ================================================================================== #
	# OUTPUT

	def fail(self):
		"""Write a failure file. Exclusively used on a node. Then exit."""
		# Save the simplest file ever and exit
		fp = open('output.txt', 'w')
		fp.write('rerr')
		fp.close()
		exit(0)

	def savesimulation(self, out):
		"""Save a simulation or averaged simulations with the same parameters."""

		if not self.parallel:
			if not hasattr(self, 'savetime'):
				import time
				self.savetime = time.strftime("%y%m%d-%H%M%S", time.localtime())

		fp = open(self._simulationtitle(), 'w')

		if not self.parallel:
			# Histograms of final population and cluster sizes
			fp.write('# --- Final population --- (N clusters / cell) ---------- #\n')
			fp.write('# Note: 0s is  % of total, >0s are % of nonzeros -------- #\n')

			for i, val in enumerate(out['final-signals-per-cell']): fp.write('%g\t%2.2f\n' % (i, val))

			fp.write('\n# --- Cluster size distribution ------------------------- #\n')
			for i, val in enumerate(out['plasmids-per-signal'][1:]): fp.write('%g\t%2.2f\n' % (i+1, val))

			# Histogram of initial population
			fp.write('\n# --- Initial population -------------------------------- #\n')
			for i, val in enumerate(out['initial-signals-per-cell']): fp.write('%g\t%2.2f\n' % (i, val))

		# Print other calculated values
		fp.write('\n# --- Other calculated values --------------------------- #\n')
		skip = ['final-signals-per-cell', 'plasmids-per-signal', 'initial-signals-per-cell']
		valstoprint = sorted([key for key in out if key not in skip])
		for key in valstoprint: fp.write('%50s: %g\n' % (key, out[key]))

		fp.write('\n# --- Parameters ---------------------------------------- #\n')
		for key in self.pars: fp.write('%50s: %g\n' % (key, self.pars[key]))
		fp.close()

	def _simulationtitle(self):
		"""Return the simulation title that has been simplified using keys defined in function savekeys."""
		if self.parallel: return 'output.txt'
		elif len(self.varying) == 0: return self.savetime + '-output.txt'
		else:
			self._savekeys()
			out = ['%s-%g' % ('abcdefghijklmnopqrstuvwxyz'[i], self.pars[key[0]]) for i, key in enumerate(self.varying)]
			return self.savetime + '/' + ', '.join(out) + '.txt'

	# Called automatically by simulationtitle
	def _savekeys(self):
		"""Filenames are simplified via keys. This is the code to those filenames."""
		import os
		if not os.path.exists(self.savetime + '/'): os.mkdir(self.savetime + '/')
		if not os.path.exists(self.savetime + '/' + 'keys.txt'):
			fk = open(self.savetime + '/' + 'keys.txt', 'w')
			fk.write('# --- Keys ---------------------------------------------- #\n')
			for i in range(len(self.varying)): fk.write('%s: %s\n' % ('abcdefghijklmnopqrstuvwxyz'[i], self.varying[i][0]))

			fk.write('\n# --- Varying ------------------------------------------- #\n')
			for i in range(len(self.varying)): fk.write('%s: %s\n' % (self.varying[i][0], str(self.varying[i][1])))
			fk.close()
		return self

	def savebest(self):
		"""Save the best parameters."""
		if len(self.varying) > 0:
			# Save the best value
			fp = open(self.savetime + '/' + 'keys.txt', 'a')
			fp.write('\n\n# --- Best values of parameters show difference of  %3g : #\n' % (self._best_comparator(self.bestpars[1])))

			keys = [key for key in self.bestpars[0]]
			keys.sort()
			for key in keys: fp.write('%50s: %g\n' % (key, self.bestpars[0][key]))
			fp.close()


	# ================================================================================== #
	# RUN SIMULATION

	def run(self):
		"""Run the simulation, taking into account all varying parameters."""
		# Determine which parameters are varying with a recursive function and simulate
		# with each possible combination of parameters. Simulations are done within
		# "vary" function instead of pre-calculating all of the possible values simply
		# to use up the minimum of space
		self.varying = []
		keys = [key for key in self.pars]
		keys.sort()
		for key in keys:
			if isinstance(self.pars[key], tuple) or isinstance(self.pars[key], list):
				self.varying.append((key, self.pars[key]))

		# self.varying is list of varying parameters, self.vary is function to vary them
		self.vary(verbose=not self.parallel)

#		if self.parallel and self.bestpars[0] == {}: self.fail()
#		elif self.parallel: self.savenode()
		if not self.parallel and self.bestpars[0] != {}: self.savebest()

	def vary(self, n=0, verbose=False):
		"""Vary the varying parameters with a recursive function."""
		if n < len(self.varying):
			x = self.varying[n][1][0]
			y = self.varying[n][1][1]
			s = self.varying[n][1][2]

			# Loop through all possible values
			while x <= y:
				self.pars[self.varying[n][0]] = x
				self.vary(n + 1, verbose)
				x += s

		else:
			# And simulate for each parameter combination
			if verbose:
				print '-'*80
				for p in self.varying: print '\tParameter %s set to %.4g' % (p[0], self.pars[p[0]])
			self.simulate()

	def simulate(self):
		"""Simulate a single run, averaging and checking output data for best."""
		# Run the simulation, averaging if necessary
		out = self._average_runs(not self.parallel)

		# Check for failed simulation
		if 'failed' not in out:
			# Save output to "best" if possible/desired
			self._compare_run_with_best(out)

			# Print
			self.savesimulation(out)

	def varied(self): return True if len(self.varying) > 0 else False



 # Class to simulate a petri dish over generations with sampling each generation
 # verbose sets whether or not output is displayed at the command line
class PetriDish:
	def __init__(self, pars, verbose=False):
 		self.pars = pars
 		self.verbose = verbose

 	def simulate(self):
		"""Simulate an experiment with a single set of parameters."""

		# Generate a population
		self.population()

		# done is used to check if there was an error during part of the generation
		done = True

		# Run the simulation to come to equilibrium for burn-in-generations
		if self.pars['burn-in-generations'] > 0:
			if self.verbose: print 'BURNING IN...'
			for i in range(self.pars['burn-in-generations']):
				if self.verbose: print '\r\tGen %g/%g' % (i + 1, self.pars['burn-in-generations'])
				if i > 0: self.sample()

				# Run S & M phases, make sure that they pass successfully
				# S & M phases are combined to account for shared cell fate across mitosis
				done = self.s_m_phase()
				if not done: return False

				# Run G1 phase, make sure that it has passed successfully
				done = self.g1phase()
				if not done: return False

			# Sample, set self.zeros to 0 (no cells with 0 plasmids), and save population
			self._burnin()

		# Run the real simulation for generations, sampling each generation
		if self.verbose: print 'SIMULATING...'
 		for i in range(self.pars['generations']):
 			if self.verbose: print '\r\tGen %g/%g' % (i + 1, self.pars['generations'])
 			if i > 0: self.sample()

 			# Find the number of cells that match selection parameters for recording
 			# the daughter-to-parent plasmid and signal ratios
 			self._set_selection_for_live_imaging_comparison()

 			# Run S & M phases
 			done = self.s_m_phase()
 			if not done: return False

 			# Run G1 phase
 			done = self.g1phase()
 			if not done: return False

 			# Save the daughter-to-parent plasmid and signal ratios
 			self._get_selected_daughters()

 		# If there were no errors, save the output
 		#if done: self.save()
 		return done

 	# ================================================================================== #
	# BIOLOGICAL FUNCTIONS
	# Biological simulation

	def s_m_phase(self):
		"""Combined S-phase and M-phase duplicates plasmids, signals, and cells and divvies up the resulting signals to daughter cells."""
		# S and M phases are combined here because we have to keep track of clusters to be
		# partitioned separately
		# Note: Ensures that all 0 values are at the end of each row

		# We want to determine which cells are duplicated by asking whether a polynomial
		# function, dependent on the number of plasmids in the cell, is less than a
		# random number.

		# Equation is c - a*x^2 - b*x^2

		x = self.n_plasmids_per_cell[:self.n]
		a = self.pars['signal-selective-disadvantage-on-cell-replication-squared']
		b = self.pars['signal-selective-disadvantage-on-cell-replication']
		c = self.pars['average-cell-replication-prob']

		# Determine which cells have duplicated
		cell_dup = -a*x*x + -b*x + c > np.random.random(self.n) # Checked the cell_dup is not 100%

		duplicated_cells = np.where(cell_dup > 0)
		if len(duplicated_cells[0]) == 0: return self.fail()

		# For each duplicated cell, duplicate plasmids
		for i in np.nditer(duplicated_cells):
			# Check if there are any plasmids in the cell, just in case
			if self.n_signals_per_cell[i] > 0:
				# Duplicate the plasmids and join aggregates in s phase
				clusters = self.sphase(i)

				# Split the clusters if necessary
				clusters = self.s_phase_cluster_breakup(clusters)

				# Split the clusters in G2-phase and distribute
				done = self.mphase(i, clusters)

				if not done:
					if self.verbose: print '\tWARNING: Hitting maximum cluster boundary in S-phase'
					return False
			self.n += 1
		return True

	def sphase(self, i):
		"""Simulate S-phase for a single cell and return duplicated aggregates and split cells."""
		# Duplicate plasmids and add to clusters
		# Generate a list of random numbers to check for plasmid duplication of length
		# n_plasmids_per_cell
		# Gives a list of 0s and 1s which can be summed
		plas_dup = self.pars['s-phase-duplication-prob'] > np.random.random(self.n_plasmids_per_cell[i]) # Checked line with print

		# Generate an array for each cluster duplicate and duplicate clusters
		clusters = np.zeros((2, self.n_signals_per_cell[i]), dtype=np.uint16)
		tally = 0
		for j in range(self.n_signals_per_cell[i]):
			clusters[0, j] = self.cells[i, j]
			clusters[1, j] = np.sum(plas_dup[tally:tally + self.cells[i, j]])
			tally += self.cells[i, j]

		# Note: clusters[0] has all of the original clusters, clusters[1] has the
		# duplicated versions which, by definition, are <= clusters[0] in size
		plas_aggregation = self.pars['plasmid-repulsion-attraction'] > np.random.random(self.n_signals_per_cell[i]) # Checked lines with print
		clusters[0, plas_aggregation] += clusters[1][plas_aggregation]
		clusters[1, plas_aggregation] = 0

		return clusters

	def mphase(self, i, clusters):
		"""Jostle clusters before mitosis, then assign signals to daughter cells."""
		# Reset cell rows
		self.cells[i, :] = 0

		# Figure out which cells each go to
		assign = np.random.randint(2, size=clusters[0].size)

		# Set counters for position in daughter cells 1 and 2
		c1 = 0; c2 = 0

		# Iterate over clusters and assign
		for j in range(clusters[0].size):
			if clusters[assign[j], j] != 0:
				self.cells[i, c1] = clusters[assign[j], j]
				c1 += 1
			if clusters[1 - assign[j], j] != 0:
				self.cells[self.n, c2] = clusters[1 - assign[j], j]
				c2 += 1

			if c1 >= self.pars['maximum-cluster-n'] - 1 or c2 >= self.pars['maximum-cluster-n'] - 1:
				return False

		# Fixed self.counts which was non-specific. Do we need both measurements
		# calculated here?
		self.n_signals_per_cell[i] = c1
		self.n_signals_per_cell[self.n] = c2

		# Make sure to maintain matches for selection
		if self.selection_passed_partner[i] > -1:
			self.selection_passed_partner[i] = self.n
			self.selection_passed_partner[self.n] = -1

		return True

	def g1phase(self):
		"""Form and break up clusters of plasmids during G1 phase."""
		for i in range(self.n):
			if self.n_signals_per_cell[i] > 0:
				out = []
				for j in range(self.n_signals_per_cell[i]):
					out += self.dissociate_cluster_and_jostle(self.cells[i, j], sphase=False)
				if len(out) >= self.pars['maximum-cluster-n']:
					if self.verbose: print '\tWARNING: Hitting maximum cluster boundary in G1 phase'
					return False

				self.cells[i, :len(out)] = out
				self.n_signals_per_cell[i] = len(out)
		return True


 	# ---------------------------------------------------------------------------------- #
	# BIOLOGICAL CLUSTERING FUNCTIONS

	def _pad(self, a, b):
		# Declare padding function to make sure that clusters stay associated
		# The first cluster of a and b are paired, all others are unpaired.

		if a == [] and b == []: return [], []
		elif a == []: return [0]*len(b), b
		elif b == []: return a, [0]*len(a)
		else:
			padto = len(a) + len(b) - 1
			a = a + [0]*(padto - len(a))
			b = [b[0]] + [0]*(padto - len(b)) + b[1:]
			return a, b

	def s_phase_cluster_breakup(self, clusters):
		"""In S-phase, takes a pair of lists of clusters and breaks them up."""
		# Clusters have already been combined, if necessary. They also have been lined up
		# so that they can have shared fates

		# Note: clusters[0] has all of the original clusters, clusters[1] has the
		# duplicated versions which, by definition, are <= clusters[0] in size

		p1 = []
		p2 = []

		for j in range(clusters[0].size):
			a = self.dissociate_cluster_and_jostle(clusters[0, j], sphase=True)
			b = self.dissociate_cluster_and_jostle(clusters[1, j], sphase=True)
			a, b = self._pad(a, b)
			p1 += a
			p2 += b

		return np.array([p1, p2], dtype=np.uint16)

	def dissociate_cluster_and_jostle(self, n, sphase=True):
		"""Cluster dissociation is a two-part process. First, check for cluster "jostling" from one parameter. If jostled, break up cluster using the Chinese Restaurant Process."""
		# Check if 1 or 0- those values have guaranteed outputs
		if n == 0: return []
		elif n == 1: return [1]
		else:
			if sphase:
				comparison = self.pars['cluster-jostling']*self.pars['cluster-jostling-s-vs-g1']
			else:
				comparison = self.pars['cluster-jostling']
			if np.random.random() > comparison: return [n]
			else: return self.crp(n, self.pars['cluster-crp-alpha'])

	def crp(self, n, alpha):
		"""Implementation of the probabilistic Chinese Restaurant Process."""

		# Return guaranteed results if possible
		if n == 0: return []
		elif n == 1: return [1]
		elif alpha >= 100: return [1]*n

		# Initialize random comparisons and return vector
		comp = np.random.random(n - 1)
		out = np.zeros(n, dtype=np.uint16)
		out[0] = 1
		mxt = 0

		# Iterate over all ns greater than 0
		for i in range(1, n):
			prob = self._crp_occupied(out[0], i + 1, alpha) # 1-indexed
			table = 0
			while prob < comp[i - 1] and table < mxt + 1:
				prob += self._crp_occupied(out[table], i+1, alpha)
				table += 1

			out[table] += 1
			if table > mxt: mxt = table
		return out[0:mxt+1].tolist()

	# Probability of joining a table of size t if it is occupied, given alpha
	def _crp_occupied(self, t, n, alpha): return float(t)/(n - 1 + alpha)


 	# ================================================================================== #
 	# SET UP FUNCTIONS
 	# Ancillary functions to set up the simulation

	def population(self):
		"""Generate an initial population and reset all of the parameters."""
		# --- Note: does not create clusters as that's too hard of a problem

		# Set the number of cells, n
		self.n = self.pars['initial-distribution-n']

		# Create the cell matrix (rows are cells, members of rows are cluster values) and set the counts per line = 0
		self.cells = np.zeros((self.n*2, self.pars['maximum-cluster-n']), dtype=np.uint16)
		self.n_plasmids_per_cell = np.zeros(self.n*2, dtype=np.uint16)
		self.n_signals_per_cell = np.zeros(self.n*2, dtype=np.uint16)

		# SELECTION-specific parameters, set to -1 for the burn in stage
		self.selection_passed_partner = np.zeros(self.n*2, dtype=np.int32) - 1

		# Set the number of cells with 0 plasmids to 0. They are simulated separately
		self.zeros = 0
		# Set the number of clusters (of size 1) per cell for each cell and add to cells
		plasnums = np.around(np.random.randn(self.n)*self.pars['initial-distribution-sd'] + self.pars['initial-distribution-x'])

		self.n_plasmids_per_cell[:self.n] = plasnums
		self.n_signals_per_cell[:self.n] = np.copy(plasnums)

		for i in range(self.n): self.cells[i, :plasnums[i]] = 1

		# Record the initial population for future comparison
		self.initpop = self._current_signals_per_cell()

		# And save the daughter-to-parent signal and plasmid ratios
		self.d_to_p_signal_ratio = []
		self.d_to_p_plasmid_ratio = []


		return self

	def sample(self):
		"""Draw a random sample of the cells so that the simulation takes a reasonable amount of time."""
		# Find the nonzero elements and only sample from them, shuffle, and set the new n
		# 15-01-26 Fixed shuffling. Tests show that shuffling works.
		nonzeros = np.nonzero(self.n_signals_per_cell[:self.n])
		np.random.shuffle(nonzeros[0])
		new_n = np.minimum(self.pars['initial-distribution-n'], nonzeros[0].size)

		# Add the 0s from the last round, sample from the nonzeros, and reset values
		self.zeros += (1.0 - self.zeros)*(1.0 - float(nonzeros[0].size)/self.n)
		self.cells[:new_n] = self.cells[np.sort(nonzeros[0][:new_n])]
		self.cells[new_n:self.n] = 0

		self.n = new_n
		self._reset_counts_per_cell()

		return self

	def _reset_counts_per_cell(self):
		"""Reset the number of signals per cell and the number of plasmids per cell."""
		# Calculate the number of clusters per cell using a sum
		# Could use the shuffled values from above, but it's harder to error check
		# The slight hit in time per generation is worthwhile
		n_clusters = np.copy(self.cells[:self.n])
		n_clusters[np.nonzero(n_clusters)] = 1
		self.n_signals_per_cell[:self.n] = np.sum(n_clusters, axis=1)
		self.n_signals_per_cell[self.n:] = 0

		# Reset the counts to plasmids per cell
		self.n_plasmids_per_cell[:self.n] = np.sum(self.cells[:self.n], axis=1)
		self.n_plasmids_per_cell[self.n:] = 0


	def _burnin(self):
		"""Reset zeros, sample, and save the initial population."""

		# Sample to account properly for 0s
		self.sample()
		# Reset zeros because we assume that the population was under selection before
		# the simulation was begun
		self.zeros = 0
		self.initpop = self._current_signals_per_cell()

	def _set_selection_for_live_imaging_comparison(self):
		"""Determine those cells with a countable number of signals for live imaging."""

		# Select all cells with counts > 0 and less than signal-maximum parameter, set value > -1
		matchedcells = np.where((self.n_signals_per_cell <= self.pars['signal-maximum']) & (self.n_signals_per_cell > 0))
		self.selection_passed_partner = np.zeros(self.n*2, dtype=np.int32) - 1
		self.selection_passed_partner[matchedcells] += 2

		# Reset the parameters
		self.parent_to_daughter_signal_ratio = np.zeros(self.n*2, dtype=np.float32)
		self.parent_to_daughter_plasmid_ratio = np.zeros(self.n*2, dtype=np.float32)

		# And set their values
		self.parent_to_daughter_signal_ratio[matchedcells] = self.n_signals_per_cell[matchedcells]
		self.parent_to_daughter_plasmid_ratio[matchedcells] = self.n_plasmids_per_cell[matchedcells]

		return self

	# COMBINED CELLS UNDER SELECTION WITH SELECTED DAUGHTERS
	def _get_selected_daughters(self):
		"""Find the daughter-to-parent signal and plasmid ratios."""

		# Recompute the number of plasmids per cell
		self.n_plasmids_per_cell[:self.n] = np.sum(self.cells[:self.n], axis=1)

		# Now pair up daughter cells
		matched = np.where(self.selection_passed_partner > -1)

		# And add to output if possible
		if len(matched[0]) > 0:
			self.parent_to_daughter_signal_ratio[matched] = (self.n_signals_per_cell[matched] + self.n_signals_per_cell[self.selection_passed_partner[matched]])/self.parent_to_daughter_signal_ratio[matched]
			self.parent_to_daughter_plasmid_ratio[matched] = (self.n_plasmids_per_cell[matched] + self.n_plasmids_per_cell[self.selection_passed_partner[matched]])/self.parent_to_daughter_plasmid_ratio[matched]

			self.d_to_p_signal_ratio.append(np.mean(self.parent_to_daughter_signal_ratio[matched]))
			self.d_to_p_plasmid_ratio.append(np.mean(self.parent_to_daughter_plasmid_ratio[matched]))

		return self



 	# ================================================================================== #
 	# OUTPUT FUNCTIONS
 	# Ancillary functions to return the results of the simulation

	def _current_signals_per_cell(self):
		"""Calculate the number of signals per cell. Used to generate the initial population."""
		# Sample to make sure that we have the correct population size and zeros are where they should be
		self.sample()

		# Calculate the histogram of the n_signals_per_cell
		cluster_bins = np.bincount(self.n_signals_per_cell).astype(np.float64)/self.n

		# Set the zeros
		if cluster_bins.size > 0: cluster_bins[0] = self.zeros
		else: cluster_bins = np.array([self.zeros])

		return cluster_bins

	def get_signals_per_cell(self, init_or_final='final'):
		"""Return the percentages of signals/cell in the initial or final population by signal size."""
		# Determine if it's the initial population or the final population and return the
		# results as percentages
		if init_or_final[0].lower() == 'i': return np.copy(self.initpop)*100.0
		else: return self._current_signals_per_cell()*100.0

	def get_plasmids_per_signal(self):
		"""Return the percentages of plasmids/signal by number of plasmids."""
		# Calculate the histogram of the number of plasmids per signal and return as
		# percentage
		cluster_size_bins = np.bincount(self.cells[:self.n].flatten().astype(np.int64)).astype(np.float64)
		cluster_size_bins[1:] = cluster_size_bins[1:]/np.sum(cluster_size_bins[1:])
		return cluster_size_bins*100.0

	def get_daughter_to_parent_signal_ratio(self):
		"""Return the mean daughter signals relative to parent signals for those cells with numbers of signals that have been selected by "signal-maximum" parameter."""
		return -1 if len(self.d_to_p_signal_ratio) < 1 else np.mean(np.array(self.d_to_p_signal_ratio))

	def get_daughter_to_parent_plasmid_ratio(self):
		"""Return the mean daughter plasmids relative to parent plasmids for those cells with numbers of signals that have been selected by "signal-maximum" parameter."""
		return -1 if len(self.d_to_p_plasmid_ratio) < 1 else np.mean(np.array(self.d_to_p_plasmid_ratio))

	# Get the difference between the final pop frequency and the initial pop frequency
	def equilibrium_change(self):
		return self.pdf_distance(np.copy(self.initpop), self._current_signals_per_cell())

	# Get the change between two population frequencies
	def pdf_distance(self, a, b, skip_zeroth=True):
		if len(a) < 2 or len(b) < 2: return 999999

		from scipy.stats import entropy
		pseudocount = 1.0/self.pars['initial-distribution-n']

		if skip_zeroth:
			a = np.copy(a)[1:]
			b = np.copy(b)[1:]

		c = np.zeros(min(50, max(len(a), len(b))))
		d = np.zeros(min(50, max(len(a), len(b))))

		c[:len(a)] = a[:50]
		d[:len(b)] = b[:50]

		c = c/np.sum(c)
		d = d/np.sum(d)

		c = (c + pseudocount)/np.sum(c + pseudocount)
		d = (d + pseudocount)/np.sum(d + pseudocount)

		return entropy(c, d) + entropy(d, c)




# Parser class is used to intelligently parse inputs without worrying about types
class Parser():
	def __init__(self): pass

	# UNKNOWN TYPE
	# Parse as int if int, float if float, else str
	def unknown(self, u):
		if u.lower() == '6.25e-06': return 0.00000625
		u = u.strip()
		if re.match('^(\d.-)+(,(\d.-)+)+$', u): return self.list(u)
		elif re.match('^\[[\d.-]+,[\d.-]+\]$', u.lower()): return self.float_range(u[1:-1])
		elif re.match('^\(((-)?[\d]+(.[\d]+)?[ \t]*,[ \t]*)*((-)?[\d]+(.[\d]+)?)\)$', u): return self.tuple(u)
		elif re.match('^(-)?[\d]+$', u.lower()): return self.int(u)
		elif re.match('^(-)?[\d.]+$', u.lower()): return self.float(u)
		elif re.match('^(-)?[-\d.e]+$', u.lower()): return self.float(u.lower()) # FIX TO MAKE MORE RESTRICTIVE
		elif re.match('^true|false$', u.lower()): return True if u[0] == 't' else False
		else: return u

	# STRING
	# Strip whitespace
	def str(self, s):
		return s.strip()

	# INTEGER
	# Strip non-integer characters and convert to int
	def int(self, i):
		i = re.sub('[^\d-]', '', i)
		return int(i)

	# FLOATING POINT
	# Strip non-float characters and convert to float
	def float(self, f):
		f = re.sub('[^\d.-]', '', f)
		return float(f)

	# TUPLE
	# Create tuple
	def tuple(self, t):
		t = t.strip()[1:-1].replace(' ', '').split(',')
		for i in range(len(t)): t[i] = self.unknown(t[i])
		return tuple(t)

	# LIST
	# Split by explicit list
	def list(self, l, splitby=','):
		l = filter(lambda x: True if x != '' else False, l.strip().split(splitby))
		for i in range(len(l)): l[i] = self.unknown(l[i])
		return l

	# FLOATING POINT RANGE
	# Split and convert
	def float_range(self, f):
		f = f.split(',')
		return [self.float(i) for i in f]


# This file can be parallelized or run locally by setting flag = 'local' or 'node'
if __name__ == "__main__":
	# Set to 'local' or 'node'
	flag = 'node'

	# Run the simulation
	r = Replicator(flag)
	r.run()

	# Graph if local and parameters were varied
	if flag == 'local' and r.varied():
		pass
		#from fgrapher import Grapher
		#g = Grapher(r.start)
		#g.graph_parameters()
		#g.graph_before_after()
