#!/bin/bash

echo "Timed Out" >> TIMEOUT.txt

echo "NODE ID: `whoami`@`hostname`" >> logs.txt
date; time timeout 3h ./ReplicatorII.py >> logs.txt
date >> logs.txt

rm -f TIMEOUT.txt
