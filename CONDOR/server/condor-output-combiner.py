import sys, os, os.path as opath


def outputvals(path):
	pars = {}
	output = {}

	with open(path, 'r') as f:
		lines = f.read().splitlines()
		foundpars = False
		for line in lines:
			if len(line) > 1:
				if line.find('Parameters') > -1: foundpars = True
				elif line.find('#') == -1 and line.find(':') > -1:
					keyval = line.strip().split(':')
					if foundpars:
						pars[keyval[0]] = keyval[1].strip()
					else:
						output[keyval[0]] = keyval[1].strip()

	return output, pars

def savetocsv(path, save, titles):
	output, pars = outputvals(path)
	if len(titles) == 0:
		titles = [sorted([key for key in output]), sorted([key for key in pars])]
		titles[1][0] = 'PARS:' + titles[1][0]
		save.write(','.join(titles[0] + titles[1]) + '\n')

	save.write(','.join([output[key] for key in titles[0]] + [pars[key] for key in titles[1]]) + '\n')
	return titles

def outputsearch(path, save, titles=[]):
	fs = os.listdir(path)
	for f in fs:
		pathf = opath.join(path, f)
		if opath.isdir(pathf):
			titles = outputsearch(pathf, save, titles)
		elif f == 'output.txt':
			titles = savetocsv(pathf, save, titles)
	return titles

if __name__ == '__main__':
	if len(sys.argv) == 1:
		print 'ERROR: Did not include path to output files.'
		exit(0)

	save_path = '150205-condor-replication-sim.csv'

	fo = open(save_path, 'w')
	outputsearch(sys.argv[1], fo)
	fo.close()
