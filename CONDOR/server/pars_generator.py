#!/bin/python
import os
#import random

mode = 'folders'
#random_mode = 'yes'
#foldermax = 50

#### DEFINE PARAMETERS ####
maximum_cluster_n = 500
burn_in_generations = 50
initial_distribution_x_LIST = [6,8,10]
initial_distribution_sd = 3
initial_distribution_n = 2000
generations = 30
s_phase_duplication_prob = 0.92
plasmid_repulsion_attraction_LIST = [1.0]
average_cell_replication_prob = 1.0
signal_selective_disadvantage_on_cell_replication = 0
signal_selective_disadvantage_on_cell_replication_squared = '0.00000625'
cluster_jostling_LIST = [0.05, 0.1, 0.15]
cluster_crp_alpha_LIST = [0.1, 0.2, 0.4]
cluster_jostling_s_vs_g1_LIST = [0, 0.05, 0.1]
averaging_runs = 10
signal_maximum_LIST = [401]

if mode == 'csv':
	i = 1
	F = open('febpars.csv', 'w')
	F.write('i, maximum_cluster_n, burn_in_generations, initial_distribution_x_value, initial_distribution_sd, initial_distribution_n, averaging_runs, generations, s_phase_duplication_prob, plasmid_repulsion_attraction_value, average_cell_replication_prob, signal_selective_disadvantage_on_cell_replication, signal_selective_disadvantage_on_cell_replication_squared, cluster_jostling_value, cluster_crp_alpha_value, cluster_jostling_s_vs_g1_value, signal_maximum_value\n')
	for cluster_jostling_value in cluster_jostling_LIST:
		for cluster_crp_alpha_value in cluster_crp_alpha_LIST:
			for cluster_jostling_s_vs_g1_value in cluster_jostling_s_vs_g1_LIST:
				for signal_maximum_value in signal_maximum_LIST:
					for initial_distribution_x_value in initial_distribution_x_LIST:
						for plasmid_repulsion_attraction_value in plasmid_repulsion_attraction_LIST:
							F.write('%07d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n' % (i, maximum_cluster_n, burn_in_generations, initial_distribution_x_value, initial_distribution_sd, initial_distribution_n, averaging_runs, generations, s_phase_duplication_prob, plasmid_repulsion_attraction_value, average_cell_replication_prob, signal_selective_disadvantage_on_cell_replication, signal_selective_disadvantage_on_cell_replication_squared, cluster_jostling_value, cluster_crp_alpha_value, cluster_jostling_s_vs_g1_value, signal_maximum_value))
							i = i+1
	F.close()
elif mode == 'folders':
	i = 1
	#foldercount = 0
	for cluster_jostling_value in cluster_jostling_LIST:
		for cluster_crp_alpha_value in cluster_crp_alpha_LIST:
			for cluster_jostling_s_vs_g1_value in cluster_jostling_s_vs_g1_LIST:
				for signal_maximum_value in signal_maximum_LIST:
					for initial_distribution_x_value in initial_distribution_x_LIST:
						for plasmid_repulsion_attraction_value in plasmid_repulsion_attraction_LIST:
							outdir = str('%07d' % i)
							if not os.path.exists(outdir):
								os.makedirs(outdir)
							outfile = str(outdir) + '/replicator-crp-pars.txt'
							F = open(outfile, 'w')
							F.write('maximum-cluster-n: %s\nburn-in-generations: %s\ninitial-distribution-x: %s\ninitial-distribution-sd: %s\ninitial-distribution-n: %s\naveraging-runs: %s\ngenerations: %s\ns-phase-duplication-prob: %s\nplasmid-repulsion-attraction: %s\naverage-cell-replication-prob: %s\nsignal-selective-disadvantage-on-cell-replication: %s\nsignal-selective-disadvantage-on-cell-replication-squared: %s\ncluster-jostling: %s\ncluster-crp-alpha: %s\ncluster-jostling-s-vs-g1: %s\nsignal-maximum: %s\n' % (maximum_cluster_n, burn_in_generations, initial_distribution_x_value, initial_distribution_sd, initial_distribution_n, averaging_runs, generations, s_phase_duplication_prob, plasmid_repulsion_attraction_value, average_cell_replication_prob, signal_selective_disadvantage_on_cell_replication, signal_selective_disadvantage_on_cell_replication_squared, cluster_jostling_value, cluster_crp_alpha_value, cluster_jostling_s_vs_g1_value, signal_maximum_value))
							F.close()
							i = i+1
