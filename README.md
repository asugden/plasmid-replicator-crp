# README #

This README details how to get simulations running locally, and on HTCondor (requires that you already have access to a HTCondor submit node)

### Running locally ###
1. Verify that you have Python 2.7 installed, as well as numpy.
2. Download source (Clone this repo)
3. Modify ReplicatorII.py lines 23 and 835 to local
4. Create Parameter files
5. Run ReplicatorII.py


### Running on HTCondor ###
For step-by-step directions on running on HTCondor please consult the wiki

### Need Assistance? ###

* Feel free to email Arthur or Mitch using the contact info in this repo.