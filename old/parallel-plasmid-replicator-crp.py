# REPLICATOR with CHINESE RESTAURANT PROCESS
# Chiu, Hayes, Sugden & Sugden
# Simulate the replication of KSHV plasmids, including clustering
# Requires the Parser class to parse inputs
# Updated: 9/26/14

import re, numpy as np
		
# Replicator class is subclassed by the Tester suite, otherwise called from this file
class Replicator:
	def __init__(self):
		# Parameter file path
		self.parnames = [
			'maximum-cluster-n',
			'burn-in-generations',
			'initial-distribution-x',
			'initial-distribution-sd',
			'initial-distribution-n',
			'generations',
			's-phase-duplication-prob',
			'plasmid-repulsion-attraction',
			'average-cell-replication-prob',
			'signal-selective-disadvantage-on-cell-replication',
			'signal-selective-disadvantage-on-cell-replication-squared',
			'cluster-jostling',
			'cluster-crp-alpha',
			'cluster-jostling-s-vs-g1',
			'averaging-runs'
		]

		self.working = True
		self.getpars()
	
	
	
	# UNKNOWN TYPE
	# Parse as int if int, float if float, else str
	def unknown(self, u):
		u = u.strip()
		if re.match('^(-)?[\d]+$', u.lower()): return self.int(u)
		elif re.match('^(-)?[\d.]+$', u.lower()): return self.float(u)
		else: return u
	
	# INTEGER
	# Strip non-integer characters and convert to int
	def int(self, i):
		i = re.sub('[^\d-]', '', i)
		return int(i)
		
	# FLOATING POINT
	# Strip non-float characters and convert to float
	def float(self, f):
		f = re.sub('[^\d.-]', '', f)
		return float(f)
		
	
	# GET PARS
	# Read all of the values from pars.txt
	def getpars(self):
		fp = open('pars.txt', 'r')
		vals = fp.read().split(',')
		fp.close()
		
		if len(vals) != len(self.parnames): return self.fail()
		
		self.pars = {}
		for i, key in enumerate(self.parnames):
			self.pars[key] = self.unknown(vals[i])
	
	# FAILURE MODE
	# Write a file with -1 as contents
	def fail(self):
		self.working = False
		fp = open('out.txt', 'w')
		fp.write('-1')
		fp.close()
		return self
	
		
	# COMBINED S PHASE AND M PHASE
	# Duplicate plasmids and cells, lose clusters, and divvy up to daughter cells
	# S and M phases are combined here because we have to keep track of clusters to be
	# partitioned separately
	# Ensures that all 0 values are at the end of each row
	def s_m_phase(self):
		# Determine which cells have duplicated, if statement speeds up process if there
		# is change in replication probability
		cell_n = np.sum(self.cells[:self.n], 1)
		cell_dup = self.pars['average-cell-replication-prob'] - cell_n*self.pars['signal-selective-disadvantage-on-cell-replication'] - cell_n*self.pars['signal-selective-disadvantage-on-cell-replication-squared']*self.pars['signal-selective-disadvantage-on-cell-replication-squared'] > np.random.random(self.n)
		
		# For each duplicated cell, duplicate plasmids
		for i in np.nditer(np.where(cell_dup > 0)):
			# Check if there are any plasmids in the cell, just in case
			if self.counts[i] > 0:
				# Duplicate the plasmids and join aggregates in s phase
				clusters = self.sphase(i)
				
				# Split the clusters in s-phase
				done = self.mphase(i, clusters)
				
				if not done:
					print '\tWARNING: Hitting maximum cluster boundary in S-phase'
					return False
			self.n += 1
		return True
		
	# S PHASE FOR A SINGLE CELL
	# Simulate S phase for a single cell and return duplicated aggregates and split cells
	def sphase(self, i):
		# Duplicate plasmids and add to clusters
		# Generate a list of random numbers to check for plasmid duplication
		plas_dup = self.pars['s-phase-duplication-prob'] > np.random.random(np.sum(self.cells[i, :self.counts[i]]))
		
		# Generate an array for each cluster duplicate and duplicate clusters
		clusters = np.zeros((2, self.counts[i]), dtype=np.uint16)
		tally = 0
		for j in range(self.counts[i]):
			clusters[0, j] = self.cells[i, j]
			clusters[1, j] = np.sum(plas_dup[tally:tally + self.cells[i, j]])
			tally += self.cells[i, j]
		
		plas_aggregation = self.pars['plasmid-repulsion-attraction'] > np.random.random(self.counts[i])
		clusters[0, plas_aggregation] += clusters[1][plas_aggregation]
		clusters[1, plas_aggregation] = 0

		return clusters
		
	# JOSTLE CLUSTERS IN S-PHASE AND ASSIGN TO CELLS
	# Split clusters and assign to daughter cells
	def mphase(self, i, clusters):
		# Reset cell rows
		self.cells[i, :self.counts[i]] = 0

		# Set counters for position in daughter cells 1 and 2
		c1 = 0; c2 = 0		
		
		# Iterate over clusters and assign
		for j in range(clusters[0].size):
			a, b = self.divvy(clusters[0][j], clusters[1][j])
			if c1 + len(a) > self.pars['maximum-cluster-n'] or c2 + len(b) > self.pars['maximum-cluster-n']: return False
			
			self.cells[i, c1:c1 + len(a)] = a
			self.cells[self.n, c2:c2 + len(b)] = b
			c1 += len(a)
			c2 += len(b)
			
		self.counts[i] = c1
		self.counts[self.n] = c2
		
		return True
			
	# DIVVY UP PAIRED PLASMIDS
	# Split clusters and assign clusters with paired fates to opposite cells
	def divvy(self, a, b):
		out = [[], []]
		# Dissociate both clusters
		a = self.cluster_dissociation(a)
		b = self.cluster_dissociation(b)
		
		if b == []:
			# Assign each of the clusters to a random daughter
			assign = np.random.randint(2, size=len(a))
			for i, v in enumerate(a):
				out[assign[i]].append(v)
		
		else:
			# First, assign the plasmids with shared fates
			assign = np.random.randint(2, size=len(a) + len(b) - 1)
			out[assign[0]].append(a[0])
			out[1 - assign[0]].append(b[0])
			
			# Then, assign the rest of the clusters randomly
			ab = a[1:] + b[1:]
			for i, v in enumerate(ab):
				out[assign[i+1]].append(v)

		return (out[0], out[1])
			
	# G1-PHASE
	# Form and break up clusters
	def g1phase(self):
		for i in range(self.n):
			if self.counts[i] == 0: break
			
			# Only duplicate  when clusters are larger than size 1
			gtones = np.where(self.cells[i, :self.counts[i]] > 1)
			if gtones[0].size ==  0: break
			
			# For every cell with clusters big enough to split (> 1), check if it's split
			split = np.random.random(gtones[0].size) < self.pars['cluster-jostling']
			if np.sum(split) > 0:
				for j, v in enumerate(self.cells[i][gtones][split]):
					# Dissociate cluster and allocate if possible
					a = self.cluster_dissociation(v, True)
					if len(a) + self.counts[i] >= self.pars['maximum-cluster-n']:
						print '\tWARNING: Hitting maximum cluster boundary in G1 phase'
						return False
					self.cells[i, j] = a[0]
					self.cells[i, self.counts[i]:self.counts[i] + len(a) - 1] = a[1:]
					self.counts[i] += len(a) - 1
					
		return True
	
	# POPULATION
	# Generate an initial population
	# --- Note: does not create clusters as that's too hard of a problem
	def population(self):
		# Set the number of cells, n
		self.n = self.pars['initial-distribution-n']
		# Create the cell matrix (rows are cells, members of rows are cluster values) and set the counts per line = 0
		self.cells = np.zeros((self.n*2, self.pars['maximum-cluster-n']), dtype=np.uint16)
		self.counts = np.zeros(self.n*2, dtype=np.uint16)
		# Set the number of cells with 0 plasmids to 0. They are simulated separately
		self.zeros = 0
		# Set the number of clusters (of size 1) per cell for each cell and add to cells
		self.plasnums = np.around(np.random.randn(self.n)*self.pars['initial-distribution-sd'] + self.pars['initial-distribution-x'])
		self.counts[:self.n] = self.plasnums
		for i in range(self.n): self.cells[i, :self.plasnums[i]] = 1
		# Record the initial population for future comparison
		self.initpop = self.proportions()[0]
		return self
		
	# BURN IN
	# Because clusters are not initially generated, the simulation can be run to come to
	# an equilibrium before continuing.
	def burnin(self):
		# Sample to account properly for 0s
		self.sample()
		# Reset zeros because we assume that the population was under selection before
		# the simulation was begun
		self.zeros = 0
		self.initpop = self.proportions()[0]
		
	# CLUSTER DISSOCIATION
	# Check for cluster dissociation and break up using the Chinese Restaurant Process
	def cluster_dissociation(self, n, jostle=False, g1=True):
		if n == 0: return []
		elif n == 1: return [1]
		elif jostle and g1 and np.random.random() >= self.pars['cluster-jostling']: return [n]
		elif jostle and np.random.random() > self.pars['cluster-jostling']*self.pars['cluster-jostling-s-vs-g1']: return [n]
		else: return self._crp(n, self.pars['cluster-crp-alpha'])
		
		
	# CHINESE RESTAURANT PROCESS
	# Pull a table arrangement from the chinese restaurant process
	def _crp(self, n, alpha):
		# Return guaranteed results if possible
		if n == 0: return []
		elif n == 1: return [1]
	
		# Initialize random comparisons and return vector
		comp = np.random.random(n - 1)
		out = np.zeros(n, dtype=np.uint16)
		out[0] = 1
		mxt = 0
	
		# Iterate over all ns greater than 0
		for i in range(1, n):
			prob = self._crp_occupied(out[0], i + 1, alpha) # 1-indexed
			table = 0
			while prob < comp[i - 1] and table < mxt + 1:
				prob += self._crp_occupied(out[table], i+1, alpha)
				table += 1
			
			out[table] += 1
			if table > mxt: mxt = table
		return out[0:mxt+1]
		
	# Probability of joining a table of size t if it is occupied, given alpha		
	def _crp_occupied(self, t, n, alpha): return float(t)/(n - 1 + alpha)

	# SAMPLE
	# Draw a random sample of the cells
	def sample(self):
		# Find the nonzero elements and only sample from them, shuffle, and set the new n
		nonzeros = np.nonzero(self.counts[:self.n])
		np.random.shuffle(nonzeros)
		new_n = np.minimum(self.pars['initial-distribution-n'], nonzeros[0].size)

		# Add the 0s from the last round, sample from the nonzeros, and reset values
		self.zeros += (1.0 - self.zeros)*(1.0 - float(nonzeros[0].size)/self.n)
		self.cells[:new_n] = self.cells[np.sort(nonzeros[0][:new_n])]
		self.cells[new_n:self.n] = 0

		self.n = new_n
		# Reset the counts
		self.counts[:self.n] = np.sum(self.cells[:self.n], axis=1)
		return self

	# PROPORTIONS
	# Return the proportions of cells with N plasmids for all N
	def proportions(self):
		# Sample to make sure that zeros are properly accounted for. Also, we don't want
		# to give a false impression of higher accuracy
		self.sample()

		# To count clusters, we set each cluster to have 1 plasmid and sum (numpy)
		# We copy self.cells so that it is not modified
		clusters = np.copy(self.cells[:self.n])
		clusters[np.nonzero(clusters)] = 1
		cluster_bins = np.bincount(np.sum(clusters, axis=1).astype(np.int64)).astype(np.float64)/self.n
		if cluster_bins.size > 0: cluster_bins[0] = self.zeros
		else: cluster_bins = np.array([self.zeros])
		
		# And we sum over the sizes of each cluster
		cluster_size_bins = np.bincount(self.cells[:self.n].flatten().astype(np.int64))[1:].astype(np.float64)
		cluster_size_bins = cluster_size_bins/np.sum(cluster_size_bins)
		
		return (cluster_bins, cluster_size_bins)

			
	# DIGEST
	# Calculate the stability of the percentage of cells with N plasmids except where N=0
	def digest(self):
		fpop = self.proportions()[0][1:]
		ipop = self.initpop[1:]
		
		fpop = (fpop/np.sum(fpop))*100
		ipop = (ipop/np.sum(ipop))*100
		
		if fpop.size < ipop.size: fpop.resize(ipop.size)
		elif ipop.size < fpop.size: ipop.resize(fpop.size)
		
		# A cheap method of just finding the difference between populations- estimate only
		diff = np.sum(np.abs(fpop - ipop))
		print '\tPopulation after %i generations changed by a sum of %3g percent' % (self.pars['generations'], diff)
		
		if diff < self.bestpars[1] or self.bestpars[1] == -1:
			bkeys = {}
			for p in self.varying: bkeys[p[0]] = self.pars[p[0]]
			self.bestpars = (bkeys, diff)
			
	# SIMULATE
	# Simulate an experiment
	def simulate(self):
		# Generate a population
		self.population()
		# done is used to check if there was an error during part of the generation
		done = True
		
		# Run the simulation to come to equilibrium for burn-in-generations
		if self.pars['burn-in-generations'] > 0:
			print 'BURNING IN...'
			for i in range(self.pars['burn-in-generations']):
				print '\r\tGen %g/%g' % (i + 1, self.pars['burn-in-generations'])
				if i > 0: self.sample()
			
				done = self.s_m_phase()
				done = self.g1phase()
				if not done: break;
			
			# Sample, set self.zeros to 0 (no cells with 0 plasmids), and save population
			self.burnin()
		
		# Run the real simulation for generations, sampling each generation
		print 'SIMULATING...'
 		for i in range(self.pars['generations']):
 			print '\r\tGen %g/%g' % (i + 1, self.pars['generations'])
 			if i > 0: self.sample()
 			
 			self.s_m_phase()
 			done = self.g1phase()
 			if not done: break;
 			
 		return done
 		
 	# AVERAGE MULTIPLE SIMULATIONS
 	def average_simulations(self):
 		av_pops = []
 		av_zeros = []
 		av_diffs = []
 		
 		for i in range(self.pars['averaging-runs']):
 			print '\t--> RUN %2i' % (i+1)
 			done = self.simulate()
 			
 			if done:
				fpop = self.proportions()[0][1:]
				ipop = self.initpop[1:]
		
				fpop = (fpop/np.sum(fpop))*100
				ipop = (ipop/np.sum(ipop))*100
		
				if fpop.size < ipop.size: fpop.resize(ipop.size)
				elif ipop.size < fpop.size: ipop.resize(fpop.size)
		
				# A cheap method of just finding the difference between populations- estimate only
				av_diffs.append(np.sum(np.abs(fpop - ipop)))
				av_zeros.append(self.zeros*100)
				av_pops.append(fpop[:20] if fpop.size >= 20 else np.append(fpop, [0 for i in range(20 - fpop.size)]))
		
		if len(av_diffs) == 0: return self.fail()
		
		av_pops = np.average(av_pops, axis=0)
		mn = np.sum(av_pops/100.0*[i+1 for i in range(av_pops.size)])
		fp = open('out.txt', 'w')
		
		# Average difference between initial pop and end pop, % zeros, peak # of plasmids, % of peak plasmids, average numpber of plasmids
		fp.write('%.2f,%.2f,%i,%.2f,%.2f' % (np.average(av_diffs), np.average(av_zeros), np.argmax(av_pops) + 1, np.max(av_pops), mn))
		fp.close()
		
		
		
# If this file is being run directly, run the simulation
if __name__ == "__main__":
	r = Replicator()
	r.average_simulations()
