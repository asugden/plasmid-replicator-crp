# PARSE.py
# Parse common inputs. Used by metadata and Physalize
# Last updated 2/21/14

import re

class Parser():
	def __init__(self):
		pass
		
	# CLI KEY VALUES
	# Split by -key or --key and values or ranges
	def keyvals(self, kv):
		args = filter(lambda x: False if x.strip() == '' or x.strip() == '-' else True, re.split('((^| )-(?=[a-z]))', kv.strip()))
		out = {}
		for arg in args:
			arg = re.sub('(\[[\d.-]+,) ([\d.-]+\])', r'\1\2', arg)
			arg = filter(lambda x: False if x == '' or x == ' ' or x == '=' else True, re.split('( |=)', arg, 1))
			if len(arg) == 1: out[self.str(arg[0]).lower()] = True
			else: out[self.str(arg[0]).lower()] = self.unknown(arg[1])
		return out
		
	# DATE OF FILE
	# Get date by name of file, file number as optional second
	def datefile(self, file):
		dt = self.filename(file).split('_')
		return ((int(dt[0])%2000, int(dt[1]), int(dt[2])), int(dt[3]))
		
	# CELL NAME BY PATH
	# Get the date and 
		
	# FILE NAME FROM PATH
	# Get name from path or return if already name
	def filename(self, path):
		end = path.rfind('.') if path.rfind('.') > 0 else len(path)
		return path[path.rfind('/') + 1:end]
		
	# STRINGIFY DATE
	# Convert date to 6-string
	def strdate(self, d):
		return '{:02g}{:02g}{:02g}'.format(d[0], d[1], d[2])
		
	# DATE
	# Return date as tuple of (year, month, day)
	# Takes input of mm/dd/yy or mm-dd-yy or yymmdd or yy mm dd
	def date(self, dp):
		dp = re.sub('[^\d\/ -]', '', dp.strip())
		ds = [
			'(^| |=)[0-9]{2,4} [0-9]{1,2} [0-9]{1,2}( |$)',
			'(^| |=)[0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}( |$)',
			'(^| |=)[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}( |$)',
			'(^| |=)[0-9]{6}( |$)',
		]
		
		if re.match(ds[3], dp):
			out = re.search(ds[3], dp).group(0)
			out = [out[0:2], out[2:4], out[4:6]]
		elif re.match(ds[0], dp):
			out = re.search(ds[0], dp).group(0)
			out = out.split(' ')
		elif re.match(ds[1], dp):
			out = re.search(ds[1], dp).group(0)
			out = out.split('/')
			out = [out[2], out[0], out[1]]
		elif re.match(ds[2], dp):
			out = re.search(ds[2], dp).group(0)
			out = out.split('-')
			out = [out[2], out[0], out[1]]
		else:
			out = []
		
		return tuple([int(i) for i in out])
	
	# KEY VAL LIST
	# Split member by tabs
	def keyvallist(self, l):
		l = filter(lambda x: True if x != '' else False, l.strip().split('\t'))
		pars = {}
		for i in range(1, len(l)):
			p = l[i].split(' ')
			if len(p) > 1: pars[p[0].strip().lower()] = self.unknown(p[1])
			else: pars[p[0].strip().lower()] = True
		return (l[0], pars)
		
	# LIST
	# Split by explicit list
	def list(self, l, splitby=','):
		l = filter(lambda x: True if x != '' else False, l.strip().split(splitby))
		for i in range(len(l)): l[i] = self.unknown(l[i])
		return l
	
	# UNKNOWN TYPE
	# Parse as int if int, float if float, else str
	def unknown(self, u):
		if u.lower() == '6.25e-06': return 0.00000625
		u = u.strip()
		if re.match('^(\d.-)+(,(\d.-)+)+$', u): return self.list(u)
		elif re.match('^\[[\d.-]+,[\d.-]+\]$', u.lower()): return self.float_range(u[1:-1])
		elif re.match('^\(((-)?[\d]+(.[\d]+)?[ \t]*,[ \t]*)*((-)?[\d]+(.[\d]+)?)\)$', u): return self.tuple(u)
		elif re.match('^(-)?[\d]+$', u.lower()): return self.int(u)
		elif re.match('^(-)?[\d.]+$', u.lower()): return self.float(u)
		elif re.match('^(-)?[-\d.e]+$', u.lower()): return self.float(u.lower()) # FIX TO MAKE MORE RESTRICTIVE
		elif re.match('^true|false$', u.lower()): return True if u[0] == 't' else False
		elif len(self.date(u.lower())) == 3: return self.date(u)
		elif len(self.date_range(u.lower())) == 3: return self.date_range(u)
		else: return u
	
	# STRING
	# Strip whitespace
	def str(self, s):
		return s.strip()
		
	# INTEGER
	# Strip non-integer characters and convert to int
	def int(self, i):
		i = re.sub('[^\d-]', '', i)
		return int(i)
		
	# FLOATING POINT
	# Strip non-float characters and convert to float
	def float(self, f):
		f = re.sub('[^\d.-]', '', f)
		return float(f)
		
	# TUPLE
	# Create tuple
	def tuple(self, t):
		t = t.strip()[1:-1].replace(' ', '').split(',')
		for i in range(len(t)): t[i] = self.unknown(t[i])
		return tuple(t)
	
	# DATE RANGE
	# Fancier version of date
	# Accepts 
	def date_range(self, dp):
		dp = re.sub('[^\d\/ -]', '',	dp.strip())
		ds = [
			'(^| |=)[0-9]{2,4}(-[0-9]{2,4})? [0-9]{1,2}(-[0-9]{1,2})? [0-9]{1,2}(-[0-9]{1,2})?( |$)',
			'(^| |=)[0-9]{1,2}(-[0-9]{1,2})?/[0-9]{1,2}(-[0-9]{1,2})?/[0-9]{2,4}(-[0-9]{2,4})?( |$)',
			'(^| |=)[0-9]{1,2}-[0-9]{1,2}-[0-9]{1,2}( |$)',
			'(^| |=)[0-9]{6}( |$)',
		]
		
		if re.match(ds[2], dp) or re.match(ds[3], dp): return self.date(dp)
		elif re.match(ds[0], dp):
			out = re.search(ds[0], dp).group(0)
			out = out.split(' ')
		elif re.match(ds[1], dp):
			out = re.search(ds[1], dp).group(0)
			out = out.split('/')
			out = [out[2], out[0], out[1]]
		else:
			out = []
		
		return tuple([[int(j) for j in i.split('-')] for i in out])
	
	# STRING RANGE
	# Split by |
	def str_range(self, s):
		s = s.split('|')
		return [self.str(i) for i in s]
		
	# INTEGER RANGE
	# Split and convert. Cannot do hyphen due to negative numbers
	def int_range(self, i):
		i = i.split(',')
		return [self.int(j) for j in i]
		
	# FLOATING POINT RANGE
	# Split and convert
	def float_range(self, f):
		f = f.split(',')
		return [self.float(i) for i in f]